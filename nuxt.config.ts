import { baseURL } from "nuxt/dist/core/runtime/nitro/paths";

export default defineNuxtConfig({
  app: {
    head: {
      link: [
        { rel: 'icon', type: 'image/x-icon', href: '/favicon.png?v=2' },
      ],
    },
  },
  modules: [
    '@nuxtjs/tailwindcss',
    '@nuxtjs/color-mode',
    '@vee-validate/nuxt',
    '@nuxtjs/i18n',
    'nuxt-lucide-icons',
    '@pinia/nuxt',
    '@vueuse/nuxt',
    "shadcn-nuxt",
    "nuxt-tiptap-editor"
  ],
  tiptap: {
    prefix: "Tiptap", //prefix for Tiptap imports, composables not included
    lowlight: {
      theme: "a11y-light",
    },
  },
  colorMode: {
    classSuffix: '',
    preference: 'light',
    fallback: 'light'
  },
  lucide: {
    namePrefix: 'Icon'
  },
  runtimeConfig: {
    public: {
      appName: process.env.APP_NAME,
      appServiceUrl: process.env.APP_SERVICE_URL,
      appService: process.env.APP_SERVICE,
    }
  },
  i18n: {
    locales: [
      {
        code: 'id',
        file: 'id-ID.ts'
      },
    ],
    lazy: true,
    langDir: 'lang',
    defaultLocale: 'id'
  },
  components: [
    '~/components/custom',
    '~/components/global',
    '~/components/pages',
  ],
  css: [
    '~/assets/css/style.css'
  ]
})