
<script setup lang="ts">
import type { Column } from "@/types";
import { LoaderCircle } from "lucide-vue-next";
import type { TableBatchAction, TableProps } from "@/types";
import type { ToRefs } from "vue";

const paginatorData = ref({
  fromRows: 1,
  toRows: 1,
  totalRows: 0,
  currentPage: 1,
  perPage: 10
});

const props = defineProps<{
  columns?: Column[];
  data?: any[];
  totalRows?: number; // For pagination
  isLoading?: boolean;
  onPageChange?: (page: number) => void; // For pagination

  checkable?: boolean; // If true, show a checkbox in the first column
  onCheckAll?: (rows: any[]) => void; // Let the parent component handle the checked rows.
                                      // For example handle to select only the visible rows or all rows
  batchActions?: TableBatchAction[]; // Actions to be executed on the checked rows
  tableProps?: ToRefs<TableProps>; // Props for the table element from the table composable
  search?: string; // Search query
  handleSort?: (column: Column) => void; // Handle sorting
  hideSearchBox?: boolean; // Hide the search box
  hideFooter?: boolean; // Hide the footer
}>();

const reactived = reactive({
  batchActions: props.batchActions,
  data: props.data,
  isLoading: props.isLoading,
  totalRows: props.totalRows,
  search: props.search
});

const emit = defineEmits(["update:checkedRows"]);

// This should be in local state because it's for the table only
const checkedRows = ref<any[]>([]);

const getNestedValue = (obj: any, path: string) => {
  return path.split(".").reduce((acc, part) => acc && acc[part], obj);
};

const prePageChanged = (page: number) => {
  paginatorData.value.currentPage = page;
  props.onPageChange?.(page);
};

const preHandleSort = (column: Column) => {
  const { columns } = props;
  const currentColumn = columns?.find((c) => c.accessor === column.accessor);

  if (currentColumn) {
    currentColumn.sortDirection = currentColumn.sortDirection === "asc" ? "desc" : "asc";
  }

  props.handleSort?.(column);
};

const debounce = (func: any, wait: number) => {
  let timeout: any;
  return function executedFunction(...args: any) {
    const later = () => {
      clearTimeout(timeout);
      func(...args);
    };
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  };
};

const debouncedSearch = debounce((value: string) => {
  const { search } = props.tableProps || toRefs({ search: "" });
  search.value = value;
}, 500);

watchEffect(() => {
  if (props.tableProps) {
    const { loading, data: tData, pageCount, pageIndex } = props.tableProps;

    reactived.isLoading = loading.value;
    reactived.data = tData.value;
    reactived.totalRows = pageCount.value;
    pageIndex.value = paginatorData.value.currentPage;
  } else {
    reactived.isLoading = props.isLoading;
    reactived.data = props.data;
    reactived.totalRows = props.totalRows;
  }

  if (reactived.data) {
    paginatorData.value.totalRows = reactived.totalRows || reactived.data?.length || 0;
    paginatorData.value.fromRows = (paginatorData.value.currentPage - 1) * paginatorData.value.perPage;
    paginatorData.value.toRows = paginatorData.value.currentPage * paginatorData.value.perPage;

    if (paginatorData.value.totalRows > 0) {
      paginatorData.value.fromRows += 1;
    }

    if (paginatorData.value.toRows > paginatorData.value.totalRows) {
      paginatorData.value.toRows = paginatorData.value.totalRows;
    }
  } else {
    paginatorData.value.totalRows = 0;
    paginatorData.value.fromRows = 0;
    paginatorData.value.toRows = 0;
  }
});

watch(reactived, () => {
  if (reactived.search) {
    debouncedSearch(reactived.search);
  } else {
    debouncedSearch("");
  }
}, { deep: true });
</script>

<template>
  <div>
    <div class="flex justify-between my-2">
      <div class="flex gap-4 items-center">
        <input
          v-if="!props.hideSearchBox"
          type="text"
          class="w-72 px-3 py-2 border border-gray-300 rounded-lg focus:outline-none focus:ring focus:ring-blue-300"
          placeholder="Search..."
          @input="(e) => {
            const inputElement = e.target as HTMLInputElement;
            reactived.search = inputElement.value;
          }"
        />

        <template v-if="batchActions?.length && checkedRows.length">
          {{ checkedRows.length }} selected
        </template>
      </div>

      <div class="button-container space-x-2">
        <Button
          v-for="action in batchActions"
          :key="action.label"
          @click="() => action.onClick(checkedRows)"
          size="sm"
          :variant="action.buttonVariant"
          v-show="checkedRows.length"
          :hidden="action.hidden()"
        >
          {{ action.label }}
        </Button>
      </div>
    </div>
    <div class="ring-1 ring-black ring-opacity-5 sm:rounded-lg">
      <div class="w-full overflow-x-auto sm:rounded-lg">
        <table class="divide-y divide-gray-300 table-auto w-full">
          <thead class="top-0 bg-gray-50">
            <tr>
              <th
                scope="col"
                width="1"
                class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900"
                v-if="props.checkable"
              >
                <Checkbox
                  id="check-all"
                  @check="isChecked => {
                    if (isChecked) {
                      checkedRows = [...data || []];
                    } else {
                      checkedRows = [];
                    }

                    emit('update:checkedRows', checkedRows);
                  }"
                  :isChecked="checkedRows.length === data?.length"
                />
              </th>

              <th
                scope="col"
                :class="`px-3 py-3.5 text-left text-sm font-semibold text-gray-900 break-all ${column.customWidth || `whitespace-nowrap`} ${column.sortable ? 'cursor-pointer' : ''}`"
                v-for="column in columns"
                :key="column.accessor"
                v-show="!column.hidden"
                @click="() => column.sortable ? preHandleSort?.(column) : null"
              >
                {{ column.value }}

                <template v-if="column.sortable">
                  <span
                    v-if="column.sortable && column.sortDirection === 'asc'"
                    class="ml-1"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      class="h-4 w-4 inline-block"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="M5 15l7-7 7 7"
                      />
                    </svg>
                  </span>
                  <span
                    v-if="column.sortable && column.sortDirection === 'desc'"
                    class="ml-1"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      class="h-4 w-4 inline-block"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="M19 9l-7 7-7-7"
                      />
                    </svg>
                  </span>

                  <!-- Default if sortable but not desc or asc is up-down -->
                  <span
                    v-if="column.sortable && !column.sortDirection"
                    class="ml-1 cursor-pointer"
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      class="h-4 w-4 inline-block"
                      fill="none"
                      viewBox="0 0 24 24"
                      stroke="currentColor"
                    >
                      <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        stroke-width="2"
                        d="M19 9l-7 7-7-7"
                      />
                    </svg>
                  </span>
                </template>
              </th>
            </tr>
          </thead>
          <tbody class="divide-y divide-gray-200 bg-white">
            <tr
              v-show="!isLoading && reactived.data?.length"
              v-for="(row, id) in reactived.data"
              :key="id"
              :class="{ 'bg-blue-100': checkedRows.includes(row) }"
            >
              <td
                class="whitespace-nowrap px-3 py-4 text-sm text-gray-500"
                v-if="props.checkable"
              >
                <Checkbox
                  :id="`check-${id}`"
                  :isChecked="checkedRows.includes(row)"
                  @check="(isChecked) => {
                    if (isChecked) {
                      checkedRows.push(row);
                    } else {
                      checkedRows.splice(checkedRows.indexOf(row), 1);
                    }

                    emit('update:checkedRows', checkedRows);
                  }"
                />
              </td>

              <td
                :class="`px-3 py-4 text-sm text-gray-500 break-all ${column.customWidth || `whitespace-nowrap`}`"
                v-for="column in columns"
                :key="column.accessor"
                v-show="!column.hidden"
              >
                <template v-if="column.customRender">
                  <template v-if="column.customRenderType === 'component'">
                    <component :is="column.customRender(row[column.accessor], row)" />
                  </template>
                </template>
                <template v-else>
                  {{ getNestedValue(row, column.accessor) || "-" }}
                </template>
              </td>
            </tr>

            <tr v-if="isLoading">
              <td
                class="whitespace-nowrap px-3 py-4 text-sm text-center text-gray-500"
                :colspan="(columns?.length ?? 0) + (props.checkable ? 1 : 0)"
              >
                <LoaderCircle class="w-6 h-6 animate-spin mx-auto" />
              </td>
            </tr>

            <tr v-if="!reactived.data?.length && !isLoading">
              <td
                class="whitespace-nowrap px-3 py-4 text-sm text-center text-gray-500"
                :colspan="columns?.length"
              >
                No data found
              </td>
            </tr>
          </tbody>
        </table>
      </div>

      <Paginator
        :fromRows="paginatorData.fromRows"
        :toRows="paginatorData.toRows"
        :totalRows="paginatorData.totalRows"
        :page="paginatorData.currentPage"
        :perPage="paginatorData.perPage"
        @update:onPageChanged="prePageChanged"
        v-show="!props.hideFooter"
      />
    </div>
  </div>
</template>
