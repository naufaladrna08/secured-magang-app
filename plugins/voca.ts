import { defineNuxtPlugin } from '#app';
import voca from 'voca';

export default defineNuxtPlugin(nuxtApp => {
  nuxtApp.provide('voca', voca);
})