const BreadcrumbConfigs = [
  {
    url: "/profile",
    pages: [
      {
        name: "Profile",
        href: "/profile",
        current: true
      }
    ]
  },
  {
    url: "/about",
    pages: [
      {
        name: "About",
        href: "/about",
        current: true
      }
    ]
  },
  {
    url: "/contact",
    pages: [
      {
        name: "Contact",
        href: "/contact",
        current: true
      }
    ]
  },
  {
    url: "/forgot-password",
    pages: [
      {
        name: "Forgot Password",
        href: "/forgot-password",
        current: true
      }
    ]
  },
  {
    url: "/reset-password",
    pages: [
      {
        name: "Reset Password",
        href: "#",
        current: true,
        disabled: true
      }
    ]
  },
  {
    url: "/login",
    pages: [
      {
        name: "Login",
        href: "/login",
        current: true
      }
    ]
  },
  {
    url: "/logbook",
    pages: [
      {
        name: "Logbook",
        href: "/logbook",
        current: true
      }
    ]
  },
  {
    url: "/logbook/final-report",
    pages: [
      {
        name: "Logbook",
        href: "/logbook",
        current: false
      },
      {
        name: "Final Report",
        href: "/logbook/final-report",
        current: true
      }
    ]
  },
  {
    url: "/logbook/offer-detail",
    pages: [
      {
        name: "Logbook",
        href: "/logbook",
        current: false
      },
      {
        name: "Offer Detail",
        href: null,
        current: false
      }
    ]
  },
  {
    url: "/lihat-selengkapnya",
    pages: [
      {
        name: "Lihat Selengkapnya",
        href: "/lihat-selengkapnya",
        current: true
      }
    ]
  },
  {
    url: "/daftar-magang",
    pages: [
      {
        name: "Daftar Magang",
        href: "/daftar-magang",
        current: true
      }
    ]
  },
  {
    url: "/daftar-magang/select-divisi",
    pages: [
      {
        name: "Daftar Magang",
        href: "/daftar-magang",
        current: true
      },
      {
        name: "Pilih Divisi",
        href: "/daftar-magang/select-divisi",
        current: true
      }
    ]
  }
];

export default BreadcrumbConfigs;