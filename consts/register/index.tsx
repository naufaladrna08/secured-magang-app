export const jobRequirements = [
  "Menjalankan seluruh rangkaian proses Program MAGNIT dengan penuh tanggung jawab, termasuk namun tidak terbatas pada proses seleksi, pembekalan, pelaksanaan, dan pembuatan laporan akhir Program.",
  "Menyediakan segala dokumen yang diperlukan sehubungan dengan pelaksanaan Program.",
  "Bersedia ditempatkan di lokasi penempatan Program yang telah ditetapkan oleh Divisi SDM & Umum.",
  "Menggunakan uang saku  yang diterima sehubungan dengan pelaksanaan Program dengan sebaik-baiknya sesuai dengan maksud dan tujuan yang telah ditetapkan oleh Kemendikbudristek.",
  "Menjaga nama baik, etika, dan citra diri sendiri, perguruan tinggi, Cabang PTP, Kemendikbudristek.",
  "Bersedia untuk menerima sanksi yang berlaku, termasuk namun tidak terbatas pada kewajiban pengembalian dana bantuan Program, pencabutan/pembatalan kepesertaan, dan tidak menerima Uang Saku, apabila saya terbukti melanggar atau tidak memenuhi ketentuan dan syarat pelaksanaan Program Kampus Merdeka."
];
