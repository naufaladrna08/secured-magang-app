import { BookX, Book, Clock, CircleCheck } from "lucide-vue-next";

const PaymentStatus = [
  { status: "pending",  icon: <BookX />, label: "Belum diproses", color: "text-black-500" },
  { status: "VERIFICATION_FILE", icon: <Book />, label: "Verifikasi Berkas", color: "text-black-500" },
  { status: "approved", icon: <CircleCheck />, label: "Sudah Ditransfer", color: "text-green-500" },
  { status: "PROCESSING", icon: <Clock />, label: "Sedang Diproses", color: "text-blue-500" },
];

export default PaymentStatus;