import type { TableProps } from "@/types";

export const useTable = ((fetchUrl: string, defaultSort: string = "created_at", mapData: any = null, defaultSortDir: string = "desc") : TableProps => {
  const data = ref([]);
  const parent = ref([]);
  const loading = ref(true);
  const error = ref(null);
  const sortColumn = ref(defaultSort);
  const sortDirection = ref(defaultSortDir);
  const search = ref("");
  const pageIndex = ref(1);
  const pageLimit = ref(10);
  const pageCount = ref(0);
  const response = ref(null);
  const additionalParams = ref({});

  const fetchData = async () => {
    loading.value = true;

    await useCustomFetch(fetchUrl, {
      method: "GET",
      headers: {
        "Content-Type": "application/json"
      }
    }, {
      page: pageIndex.value,
      limit: pageLimit.value,
      sort: sortColumn.value,
      order: sortDirection.value,
      q: search.value,
      ...additionalParams.value
    }).then((res: any) => {
      data.value = mapData ? mapData(res?.data?.data) : res?.data?.data;
      pageCount.value = res?.data?.total;
      error.value = null;
      response.value = res;

      if (res?.parent) {
        parent.value = res?.parent;
      }
    }).catch((error) => {
      error.value = error;
    }).finally(() => {
      loading.value = false;
    });
  }

  const sortChanged = (column: string) => {
    if (sortColumn.value === column) {
      sortDirection.value = sortDirection.value === "asc" ? "desc" : "asc";
    } else {
      sortColumn.value = column;
      sortDirection.value = "asc";
    }
  };

  const reload = () => {
    fetchData()
  };

  onMounted(() => {
    fetchData()
  });

  watch([pageIndex, pageLimit, sortColumn, sortDirection], () => {
    fetchData()
  });

  watch(search, () => {
    pageIndex.value = 1;
    fetchData()
  });

  return {
    data,
    loading,
    error,
    sortColumn,
    sortDirection,
    search,
    pageIndex,
    pageLimit,
    pageCount,
    sortChanged,
    reload,
    response,
    parent,
    additionalParams,
  }
});
