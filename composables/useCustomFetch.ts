export const useCustomFetch = (url: any, opts: any = null, query: any = null) => {
  const runtimeConfig = useRuntimeConfig();
  const xsrfToken = useCookie('XSRF-TOKEN')
  let headers = {
    accept: 'application/json',
    ...opts?.headers,
  }
  if (xsrfToken && xsrfToken.value !== null) {
    headers['X-XSRF-TOKEN'] = xsrfToken.value;
  }

  if (process.server) {
    headers = {
      ...headers,
      ...useRequestHeaders(['cookie']),
      referer: runtimeConfig.public.baseURL
    }
  }

  if (query) {
    url = new URL(url, runtimeConfig.public.appServiceUrl);
    Object.keys(query).forEach(key => url.searchParams.append(key, query[key]))
  }

  return $fetch(url, {
    baseURL: query ? '' : runtimeConfig.public.appServiceUrl,
    headers,
    credentials: 'include',
    ...opts,
  })
}