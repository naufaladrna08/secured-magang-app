export const useHeadTitle = ((currentPageTitle: string) => {
  const runtimeConfig = useRuntimeConfig()
  
  useHead({
    title: `${currentPageTitle} - ${runtimeConfig.public.appName}`
  })
})