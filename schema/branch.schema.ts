import { z } from 'zod';

export const Branch = z.object({
  name: z
    .string({ invalid_type_error: "Please enter a valid data" })
    .min(3, "Minimum length is 3")
    .max(100, "Maximum length is 100"),
  cover: z
    .any()
});