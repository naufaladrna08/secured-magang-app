import zod from "zod";

export const CreateOffer = zod.object({
  divisionId: zod.number(),
  dates: zod
    .object({
      startDate: zod.string(),
      endDate: zod.string(),
      closingDate: zod.string(),
      announcementDate: zod.string(),
    })
    .partial()
    .superRefine((data, ctx) => {
      if (
        new Date(data.startDate as string) > new Date(data.endDate as string)
      ) {
        ctx.addIssue({
          code: zod.ZodIssueCode.custom,
          message: "Start date should be less than End date",
          path: ["startDate", "endDate"],
        });
      }

      if (
        new Date(data.closingDate as string) >
        new Date(data.announcementDate as string)
      ) {
        ctx.addIssue({
          code: zod.ZodIssueCode.custom,
          message: "Closing date should be less than Announcement date",
          path: ["closingDate", "announcementDate"],
        });
      }
    }),
  isCertified: zod.boolean().optional(),
  isAllUniversity: zod.boolean().optional(),
  minimumEducation: zod.string(),
  universities: zod.array(zod.object({})).optional(),
  remark: zod.string().optional(),
  overallQuota: zod.number().optional(),
});
