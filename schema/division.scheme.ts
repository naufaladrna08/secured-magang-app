import { z } from 'zod';

export const Division = z.object({
  bid: z.any(),
  name: z
    .string({ invalid_type_error: "Please enter a valid data" })
    .min(3, "Minimum length is 3")
    .max(100, "Maximum length is 100"),
  description: z
    .string({ invalid_type_error: "Please enter a valid data" })
    .min(3, "Minimum length is 3")
    .max(150, "Maximum length is 150"),
  address: z
    .string({ invalid_type_error: "Please enter a valid data" })
    .min(3, "Minimum length is 3")
    .max(150, "Maximum length is 150"),
  icon: z
    .any()
});