import { z } from 'zod';

export const Admin = z.object({
  nipp: z.number().default(0),
  username: z.string().min(6).max(100),
  name: z.string().min(3).max(100),
  email: z.string().email(),
  password: z.string().min(6),
  unit: z.string().min(3).max(100),
  role: z.string().min(3).max(100),
  // divisiIds: z.array(z.any()).optional(),
  divisiIds: z.number().optional(),
})
