import { z } from 'zod'

export const LoginSchema = z.object({
  emailOrUsername: z
    .string({ invalid_type_error: "Please enter a valid username or email" })
    .min(3, "Minimum username or email length is 3")
    .max(100, "Maximum username or email length is 100"),
  password: z
    .string({ invalid_type_error: "Please enter a valid password"})
    .min(3, "Minimum password length is 3")
    .max(100, "Maximum password length is 100")
})
