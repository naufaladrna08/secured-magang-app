import zod from "zod";

export const MenuSchema = zod.object({
  name: zod.string().min(3, "Label must be at least 3 characters").max(255),
  url: zod.string(),
  icon: zod.string(),
});