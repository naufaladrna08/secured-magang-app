export type DualListBoxOption = {
  id: string;
  label: string;
  value: string;
};