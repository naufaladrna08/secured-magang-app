export type Column = {
  accessor: string;
  value: string;
  customRender?: (value: any, row: any) => Component | VNode;
  customRenderType?: string;
  hidden?: boolean; // default: false. For retreiving data from API but not showing it in the table
  customWidth?: string;
  sortable?: boolean; // default: false
  sortDirection?: "asc" | "desc"; // default: "asc"
};

export type TableBatchAction = {
  label: string;
  onClick: (rows: any[]) => void;
  buttonVariant?: "primary" | "secondary" | "danger" | "warning" | "success" | "white" | "outlinePrimary";
  hidden: () => boolean | false; // default: false
};