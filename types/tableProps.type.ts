import type { Column } from "./table.type";

export type TableProps = {
  loading: Ref<boolean>;
  data: Ref<any[]>;
  error: Ref<any>;
  sortColumn: Ref<string>;
  sortDirection: Ref<string>;
  search: Ref<string>;
  pageIndex: Ref<number>;
  pageLimit: Ref<number>;
  pageCount: Ref<number>;
  reload: () => void;
  sortChanged: (column: string) => void;
  response: Ref<any>;
  parent: Ref<any>;
  additionalParams: Ref<any>;
};
