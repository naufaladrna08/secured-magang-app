export type User = {
  id: number;
  username: string;
  name: string;
  email: string;
  campus_email: string;
  email_verified_at: Date;
  nim: string;
  photo: string;
  created_at: Date;
  updated_at: Date;
  message?: string;
};