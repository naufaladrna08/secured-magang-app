export type RegisterStatus = {
  id?: number;
  branch?: string;
  division?: string;
  city?: string;
  startDate?: string;
  endDate?: string;
  type?: string;
  announcementDate?: string;
  closingDate?: string;
  status?: string;
  workingLocation?: string;
  workingLocationDescription?: string;
  step?: string;
  internshipMonth?: number;
  interviewDate?: string;
  interviewTime?: string;
  interviewLink?: string;
  certificate?: string;
}