export type Credentials = {
  emailOrUsername?: string;
  password?: string;
};