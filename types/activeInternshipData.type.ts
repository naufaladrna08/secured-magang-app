export type ActiveInternshipType = {
  isAvailable?: boolean;
  branch?: string;
  division?: string;
  period?: string;
  start_date?: string;
  end_date?: string;
  raw_end_date?: string;
  internship_month?: number;
  offer_id?: number;
};
