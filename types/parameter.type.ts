export type Parameter = {
  id: number;
  type: string;
  code: string;
  name: string;
  description: string;
};