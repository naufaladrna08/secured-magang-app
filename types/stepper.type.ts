export type Step = {
  title: string;
  description: string;
  status: 'active' | 'disabled' | 'completed' | 'expired';
};