export default {
  login: "Masuk",
  logout: "Keluar",
  register: "Daftar",
  about: "Tentang Magnit",
  home: "Beranda",
  registerInternship: "Daftar Magang",
  seeMore: "Lihat Selengkapnya",
  logbook: "Logbook",

  // Login form
  emailOrUsername: "Email",
  password: "Kata Sandi",
  nullCredentialsMessage: "Email dan kata sandi tidak boleh kosong",
  invalidCredentialsMessage: "Email atau kata sandi salah",
  emptyPasswordMessage: "Kata sandi tidak boleh kosong",
  loginSuccessMessage: "Berhasil masuk",

  // Select Divisi
  availableSlots: "Slot Tersedia",
  chooseDivision: "Pilih Divisi",
  internshipDuration: "Durasi Magang",
  closing: "Penutupan",
  announcement: "Pengumuman",
  internshipPeriod: "Periode Magang",
  internshipTimeline: "Timeline Magang",
  educationalLevel: "Jenjang Pendidikan",
}