import { get } from "@vueuse/core";
import { defineStore } from "pinia";
import type { User, Credentials } from "~/types";

export const useAuth = defineStore('auth', {
  state: () => ({
    user: null as User | null,
    authenticated: false,
  }),
  actions: {
    async signIn(credentials: Credentials) {
      await useCustomFetch('/sanctum/csrf-cookie');

      const data = await useCustomFetch('/alogin', {
        method: 'POST',
        body: credentials
      });

      if (data.code === 200) {
        const token = useCookie('token');
        token.value = data?.data?.token?.plainTextToken; // set token to cookie
        this.user = data.data.user as User;
        this.authenticated = true;
      }
      
      return data;
    },
    async signOut() {
      const token = useCookie('token');
      const data = await useCustomFetch('/alogout', {
        method: "POST"
      });
      
      if (data.code === 200) {
        token.value = null; // remove token from cookie
        this.user = null;
        this.authenticated = false;
      }
    },
    async getUser(): Promise<User | null> {
      const data = await useCustomFetch('/user');
      const user = data as User;
      return user.message === "Unauthenticated." ? null : user;
    },
    async getPermissions() {
      const data = await useCustomFetch('/admin/get-user-permissions')
        .catch((error) => {
          if (error.response?.status === 401) {
            return "Unauthorized";
          }
        });
  
      return data || null;
    },
    async getRoles() {
      const data = await useCustomFetch('/admin/get-assigned-user-role');
      return data?.data || null;
    },
    async getAuthenticated() {
      const data = await useCustomFetch('/user');
      const user = data as User;
      return user.message === "Unauthenticated." ? false : true;
    },
    async getActiveInternship() {
      const data = await useCustomFetch('/get-active-internship');
      return data || null;
    }
  }
})