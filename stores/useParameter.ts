import type { Parameter } from '@/types';

export const useParameter = defineStore('parameter', {
  state: () => ({
    parameters: [] as Parameter[] | [],
  }),
  actions: {
    setParameters(parameters: Parameter[]) {
      this.parameters = parameters;
    },
    getParameterByCode(code: string) {
      return this.parameters.find((parameter) => parameter.code === code)?.description;
    }
  },
});