import { storeToRefs } from 'pinia';

export default defineNuxtRouteMiddleware(async (to) => {
  const { user, authenticated } = storeToRefs(useAuth());
  const token = useCookie('token');
  const { getUser } = useAuth();

  // Only fetch the user if not already authenticated and a token exists
  if (!authenticated.value && token.value) {
    try {
      if (!user.value) { // Ensure user data isn't already available
        const data = await getUser();
        user.value = data;
      }
      authenticated.value = true;
    } catch (error) {
      authenticated.value = false;
      console.error(error);
    }
  }
});
